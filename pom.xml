<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.opencds</groupId>
	<artifactId>fhir-kb</artifactId>
	<version>1.0.1</version>

	<properties>

		<kie.version>6.2.0.Final</kie.version>

		<!-- enable java 1.7 -->
		<maven.compiler.source>1.7</maven.compiler.source>
		<maven.compiler.target>1.7</maven.compiler.target>

		<!-- groovy -->
		<groovy.version>2.3.8</groovy.version>
		<groovy.eclipse.compiler.version>2.9.0-01</groovy.eclipse.compiler.version>
		<groovy.eclipse.batch.version>2.3.7-01</groovy.eclipse.batch.version>

		<!-- jaxb -->
		<jaxb.api.version>2.2.12</jaxb.api.version>

		<!-- testing - uses groovy -->
		<spock.version>0.7-groovy-2.0</spock.version>
		<junit.version>4.12</junit.version>
		<xml.unit.version>1.5</xml.unit.version>
		<cglib.version>3.1</cglib.version>
		<cglib.nodep.version>3.1</cglib.nodep.version>

		<!-- spring -->
		<spring.version>4.1.6.RELEASE</spring.version>
		<spring.security.version>3.2.5.RELEASE</spring.security.version>
		<spring.ws.core.version>2.2.0.RELEASE</spring.ws.core.version>

		<!-- xml libs -->
		<jaxb.api.version>2.2.11</jaxb.api.version>
		<xerces.impl.version>2.11.0</xerces.impl.version> <!-- Needs this specific version -->

		<!-- other libs -->
		<commons.pool2.version>2.2</commons.pool2.version>
		<commons.lang3.version>3.3.2</commons.lang3.version>
		<quartz.version>2.2.1</quartz.version>

		<!-- google -->
		<google.guava.version>18.0</google.guava.version>

		<!-- logging libs -->
		<commons.logging.version>1.2</commons.logging.version>
		<slf4j.version>1.7.9</slf4j.version>

		<jaxrs.api.version>2.0.1</jaxrs.api.version>
		<jersey.version>2.14</jersey.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.drools</groupId>
				<artifactId>drools-bom</artifactId>
				<type>pom</type>
				<version>${kie.version}</version>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>

		<!-- drools kie -->
		<dependency>
			<groupId>org.kie</groupId>
			<artifactId>kie-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.drools</groupId>
			<artifactId>drools-compiler</artifactId>
			<scope>runtime</scope>
		</dependency>

		<!-- bpmn2 -->
		<dependency>
			<groupId>org.jbpm</groupId>
			<artifactId>jbpm-bpmn2</artifactId>
			<version>${kie.version}</version>
		</dependency>

		<!-- hapi fhir -->
		<dependency>
			<groupId>ca.uhn.hapi.fhir</groupId>
			<artifactId>hapi-fhir-base</artifactId>
			<version>1.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>ca.uhn.hapi.fhir</groupId>
			<artifactId>hapi-fhir-structures-dstu2</artifactId>
			<version>1.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>ca.uhn.hapi</groupId>
			<artifactId>hapi-osgi-base</artifactId>
			<version>2.0</version>
		</dependency>

		<!-- eclipse core -->
		<dependency>
			<groupId>org.eclipse.core</groupId>
			<artifactId>org.eclipse.core.runtime</artifactId>
			<version>3.7.0</version>
		</dependency>

		<!-- groovy -->
		<dependency>
			<groupId>org.codehaus.groovy</groupId>
			<artifactId>groovy-all</artifactId>
			<version>${groovy.version}</version>
		</dependency>

		<!-- jaxb -->
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>${jaxb.api.version}</version>
		</dependency>

		<!-- spock -->
		<dependency>
			<groupId>org.spockframework</groupId>
			<artifactId>spock-core</artifactId>
			<version>${spock.version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.codehaus.groovy</groupId>
					<artifactId>groovy-all</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.hamcrest</groupId>
					<artifactId>hamcrest-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.spockframework</groupId>
			<artifactId>spock-spring</artifactId>
			<version>${spock.version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.codehaus.groovy</groupId>
					<artifactId>groovy-all</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.hamcrest</groupId>
					<artifactId>hamcrest-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- xml -->
		<dependency>
			<groupId>xerces</groupId>
			<artifactId>xercesImpl</artifactId>
			<version>${xerces.impl.version}</version>
		</dependency>

		<!-- Test Dependencies -->
		<dependency>
			<groupId>org.codehaus.groovy</groupId>
			<artifactId>groovy-test</artifactId>
			<version>${groovy.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.codehaus.groovy</groupId>
			<artifactId>groovy-xml</artifactId>
			<version>${groovy.version}</version>
		</dependency>
		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib-nodep</artifactId>
			<version>${cglib.nodep.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>xmlunit</groupId>
			<artifactId>xmlunit</artifactId>
			<version>${xml.unit.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- other libs -->
		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib</artifactId>
			<version>${cglib.version}</version>
			<type>jar</type>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons.lang3.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-pool2</artifactId>
			<version>${commons.pool2.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-expression</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
			<version>${spring.security.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-aop</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-beans</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-context</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-expression</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${spring.security.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-aop</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-beans</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-context</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${spring.security.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-context</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.ws</groupId>
			<artifactId>spring-ws-core</artifactId>
			<version>${spring.ws.core.version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-aop</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-expression</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.springframework</groupId>
					<artifactId>spring-context</artifactId>
				</exclusion>
				<exclusion>
					<groupId>wsdl4j</groupId>
					<artifactId>wsdl4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz</artifactId>
			<version>${quartz.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<repositories>
		<repository>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<id>chpc-nexus</id>
			<name>CHPC Nexus</name>
			<url>https://nexus.chpc.utah.edu/content/groups/public</url>
		</repository>

		<repository>
			<id>central</id>
			<url>http://repo1.maven.org/maven2/</url>
		</repository>
	</repositories>

</project>
