package org.socraticgrid.fhir;

import groovy.xml.XmlUtil

import org.socraticgrid.fhir.generated.Citizenship
import org.socraticgrid.fhir.generated.QICoreConditionAdapter
import org.socraticgrid.fhir.generated.QICorePatientAdapter

import spock.lang.Specification
import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt
import ca.uhn.fhir.model.dstu2.composite.CodingDt
import ca.uhn.fhir.model.dstu2.composite.TimingDt
import ca.uhn.fhir.parser.IParser
import ca.uhn.fhir.model.api.ResourceMetadataKeyEnum
import ca.uhn.fhir.model.api.*
import ca.uhn.fhir.model.api.Bundle

class QICorePatientFunctionalSpec extends Specification {

	private static final samplesPath = "src/test/resources/samples"
	
	def "create patient"(){
		setup:
		
		QICoreConditionAdapter condition = new QICoreConditionAdapter()
		Date date = new Date()
		Calendar c = Calendar.getInstance()
		c.setTime(date)
		def dateNow = c.add(Calendar.MONTH, -3)
		
		condition.setOnsetDateTime(dateNow)
		
		CodeableConceptDt codeableConceptDt = new CodeableConceptDt()
		CodingDt codingDt = new CodingDt()		
		codingDt.setCode("73211009")
		codingDt.setDisplay("Diabetes mellitus (disorder)")
		codingDt.setSystem("SNOMED-CT")
		
		codeableConceptDt.getCoding().add(codingDt)
		//CodeDt codeDt = new CodeDt()
		condition.setCode(codeableConceptDt)
		
		QICorePatientAdapter patient = new QICorePatientAdapter()
		((TagList) patient.getAdaptee().getResourceMetadata().get(ResourceMetadataKeyEnum.TAG_LIST)).add(new Tag("http://hl7.org/fhir/tag/profile", "http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient"))
		Tag tag = ((TagList) patient.getAdaptee().getResourceMetadata().get(ResourceMetadataKeyEnum.TAG_LIST)).getTag("http://hl7.org/fhir/tag/profile", "http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient")
		System.out0.println("Tag": tag.toString())
		CodeableConceptDt religion = new CodeableConceptDt()
		CodingDt religionCode = new CodingDt("http://thedevine", "Spiritual")
		List<Citizenship> citizenships = new ArrayList<Citizenship>()
		Citizenship citizenship = new Citizenship()
		CodeableConceptDt codeableConcept = new CodeableConceptDt()
		CodingDt coding = new CodingDt("http://snomedct", "234")
		TimingDt timing = new TimingDt()
		timing.addEvent(new Date())

		when:
		religion.getCoding().add(religionCode)
		patient.setReligion(religion)
		patient.setActive(true)
		patient.setBirthDate(new Date())

		coding.setDisplay("My first code")
		codeableConcept.getCoding().add(coding)
		citizenship.setCode(codeableConcept)

		citizenship.setPeriod(timing)
		citizenships.add(citizenship)
		patient.setCitizenship(citizenships)
		
		

		FhirContext context = FhirContext.forDstu2()
		IParser parser = context.newXmlParser()
		
		// add resources to bundle
		String theServerBase = ""  // assume this is okay here; not sure
		Bundle bundle = new ca.uhn.fhir.model.api.Bundle()
		bundle.addResource(patient.getAdaptee(), context, theServerBase);
		
		//String patientXml = parser.encodeResourceToString(patient.getAdaptee())
		String bundleXml = parser.encodeBundleToString(bundle)

		then:
		bundleXml != null
		
		// print and save xml
		def prettyXml = XmlUtil.serialize(bundleXml)
		System.out.println(prettyXml)
		new java.io.FileWriter("${samplesPath}/bundle.xml").withWriter{it << prettyXml}
	}
}
