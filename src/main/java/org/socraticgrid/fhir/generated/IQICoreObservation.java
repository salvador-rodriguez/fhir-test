package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.dstu2.composite.RangeDt;
import ca.uhn.fhir.model.dstu2.composite.RatioDt;
import ca.uhn.fhir.model.dstu2.composite.SampledDataDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.primitive.TimeDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationValueAbsentReasonEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationInterpretationCodesEnum;
import ca.uhn.fhir.model.primitive.InstantDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationReliabilityEnum;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;

public interface IQICoreObservation
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public CodeableConceptDt getBodyPosition();

   public void setBodyPosition(CodeableConceptDt param);

   public CodeableConceptDt getDelta();

   public void setDelta(CodeableConceptDt param);

   public CodeableConceptDt getCode();

   public void setCode(CodeableConceptDt param);

   public CodeableConceptDt getBodySiteCodeableConcept();

   public void setBodySiteCodeableConcept(CodeableConceptDt param);

   public ResourceReferenceDt getBodySiteReference();

   public void setBodySiteReference(ResourceReferenceDt param);

   public QuantityDt getValueQuantity();

   public void setValueQuantity(QuantityDt param);

   public CodeableConceptDt getValueCodeableConcept();

   public void setValueCodeableConcept(CodeableConceptDt param);

   public StringDt getValueStringElement();

   public String getValueString();

   public void setValueString(StringDt param);

   public void setValueString(String param);

   public RangeDt getValueRange();

   public void setValueRange(RangeDt param);

   public RatioDt getValueRatio();

   public void setValueRatio(RatioDt param);

   public SampledDataDt getValueSampledData();

   public void setValueSampledData(SampledDataDt param);

   public AttachmentDt getValueAttachment();

   public void setValueAttachment(AttachmentDt param);

   public TimeDt getValueTimeElement();

   public String getValueTime();

   public void setValueTime(TimeDt param);

   public void setValueTime(String param);

   public DateTimeDt getValueDateTimeElement();

   public Date getValueDateTime();

   public void setValueDateTime(DateTimeDt param);

   public void setValueDateTime(Date param);

   public TimingDt getValuePeriod();

   public void setValuePeriod(TimingDt param);

   public BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> getDataAbsentReason();

   public void setDataAbsentReason(
         BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> param);

   public BoundCodeableConceptDt<ObservationInterpretationCodesEnum> getInterpretation();

   public void setInterpretation(
         BoundCodeableConceptDt<ObservationInterpretationCodesEnum> param);

   public StringDt getCommentsElement();

   public String getComments();

   public void setComments(String param);

   public void setComments(StringDt param);

   public DateTimeDt getAppliesDateTimeElement();

   public Date getAppliesDateTime();

   public void setAppliesDateTime(DateTimeDt param);

   public void setAppliesDateTime(Date param);

   public TimingDt getAppliesPeriod();

   public void setAppliesPeriod(TimingDt param);

   public InstantDt getIssuedElement();

   public Date getIssued();

   public void setIssued(Date param);

   public void setIssued(InstantDt param);

   public String getStatus();

   public void setStatus(String param);

   public BoundCodeDt<ObservationStatusEnum> getStatusElement();

   public void setStatus(BoundCodeDt<ObservationStatusEnum> param);

   public String getReliability();

   public void setReliability(String param);

   public BoundCodeDt<ObservationReliabilityEnum> getReliabilityElement();

   public void setReliability(BoundCodeDt<ObservationReliabilityEnum> param);

   public CodeableConceptDt getMethod();

   public void setMethod(CodeableConceptDt param);

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public Observation getAdaptee();

   public void setAdaptee(Observation param);
}