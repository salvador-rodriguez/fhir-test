package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.IQICorePatient;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.AddressDt;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.ContactPointDt;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DateDt;
import java.util.Date;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.dstu2.valueset.MaritalStatusCodesEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.primitive.IntegerDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.api.ExtensionDt;

public class QICorePatientAdapter implements IQICorePatient
{

   private Patient adaptedClass = new Patient();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public List<CodeableConceptDt> getRace() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-race");
		List<ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			returnList
					.add((ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extension
							.getValue());
		}
		return returnList;
	}

   public void setRace(List<CodeableConceptDt> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < ((List<ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt>) param)
               .size(); index++)
         {
            adaptedClass
                  .addUndeclaredExtension(
                        false,
                        "http://hl7.org/fhir/StructureDefinition/us-core-race",
                        (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) param
                              .get(index));
         }
      }
   }

   public CodeableConceptDt getEthnicity()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-ethnicity");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for ethnicity");
      }
   }

   public void setEthnicity(CodeableConceptDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            param);
   }

   public CodeableConceptDt getReligion()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-religion");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for religion");
      }
   }

   public void setReligion(CodeableConceptDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/us-core-religion",
            param);
   }

   public AddressDt getBirthPlace()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/birthPlace");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.AddressDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for birthPlace");
      }
   }

   public void setBirthPlace(AddressDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/birthPlace", param);
   }

   public List<CodeableConceptDt> getDisability() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-disability");
		List<ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			returnList
					.add((ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extension
							.getValue());
		}
		return returnList;
	}

   public void setDisability(List<CodeableConceptDt> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < ((List<ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt>) param)
               .size(); index++)
         {
            adaptedClass
                  .addUndeclaredExtension(
                        false,
                        "http://hl7.org/fhir/StructureDefinition/patient-disability",
                        (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) param
                              .get(index));
         }
      }
   }

   public CodeableConceptDt getImportance()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-importance");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for importance");
      }
   }

   public void setImportance(CodeableConceptDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/patient-importance",
            param);
   }

   public List<Nationality> getNationality() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-nationality");
		List<org.socraticgrid.fhir.generated.Nationality> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			org.socraticgrid.fhir.generated.Nationality item = new org.socraticgrid.fhir.generated.Nationality();
			item.setRootObjectExtension(extension);
			returnList.add(item);
		}
		return returnList;
	}

   public void setNationality(List<Nationality> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < param.size(); index++)
         {
            adaptedClass.addUndeclaredExtension(param.get(index)
                  .getRootObjectExtension());
         }
      }
   }

   public List<Citizenship> getCitizenship() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-citizenship");
		List<org.socraticgrid.fhir.generated.Citizenship> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			org.socraticgrid.fhir.generated.Citizenship item = new org.socraticgrid.fhir.generated.Citizenship();
			item.setRootObjectExtension(extension);
			returnList.add(item);
		}
		return returnList;
	}

   public void setCitizenship(List<Citizenship> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < param.size(); index++)
         {
            adaptedClass.addUndeclaredExtension(param.get(index)
                  .getRootObjectExtension());
         }
      }
   }

   public BooleanDt getCadavericDonor()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-cadavericDonor");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) extensions.get(0)
               .getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for cadavericDonor");
      }
   }

   public void setCadavericDonor(BooleanDt param)
   {
      adaptedClass
            .addUndeclaredExtension(
                  false,
                  "http://hl7.org/fhir/StructureDefinition/patient-cadavericDonor",
                  param);
   }

   public CodeableConceptDt getVeteranMilitaryStatus()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-veteranMilitaryStatus");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for veteranMilitaryStatus");
      }
   }

   public void setVeteranMilitaryStatus(CodeableConceptDt param)
   {
      adaptedClass
            .addUndeclaredExtension(
                  false,
                  "http://hl7.org/fhir/StructureDefinition/patient-veteranMilitaryStatus",
                  param);
   }

   public CodeableConceptDt getAdoptionInfo()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-adoptionInfo");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for adoptionInfo");
      }
   }

   public void setAdoptionInfo(CodeableConceptDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/patient-adoptionInfo",
            param);
   }

   public CodeableConceptDt getCauseOfDeath()
   {
      List<ca.uhn.fhir.model.api.ExtensionDt> extensions = adaptedClass
            .getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-causeOfDeath");
      if (extensions == null || extensions.size() <= 0)
      {
         return null;
      }
      else if (extensions.size() == 1)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) extensions
               .get(0).getValue();
      }
      else
      {
         throw new RuntimeException(
               "More than one extension exists for causeOfDeath");
      }
   }

   public void setCauseOfDeath(CodeableConceptDt param)
   {
      adaptedClass.addUndeclaredExtension(false,
            "http://hl7.org/fhir/StructureDefinition/patient-causeOfDeath",
            param);
   }

   public List<ClinicalTrial> getClinicalTrial() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial");
		List<org.socraticgrid.fhir.generated.ClinicalTrial> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			org.socraticgrid.fhir.generated.ClinicalTrial item = new org.socraticgrid.fhir.generated.ClinicalTrial();
			item.setRootObjectExtension(extension);
			returnList.add(item);
		}
		return returnList;
	}

   public void setClinicalTrial(List<ClinicalTrial> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < param.size(); index++)
         {
            adaptedClass.addUndeclaredExtension(param.get(index)
                  .getRootObjectExtension());
         }
      }
   }

   public List<DateTimeDt> getBirthTime() {
		List<ExtensionDt> extensions = adaptedClass
				.getUndeclaredExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-birthTime");
		List<ca.uhn.fhir.model.primitive.DateTimeDt> returnList = new java.util.ArrayList<>();
		for (ExtensionDt extension : extensions) {
			returnList.add((ca.uhn.fhir.model.primitive.DateTimeDt) extension
					.getValue());
		}
		return returnList;
	}

   public void setBirthTime(List<DateTimeDt> param)
   {
      if (param != null && param.size() > 0)
      {
         for (int index = 0; index < ((List<ca.uhn.fhir.model.primitive.DateTimeDt>) param)
               .size(); index++)
         {
            adaptedClass
                  .addUndeclaredExtension(
                        false,
                        "http://hl7.org/fhir/StructureDefinition/patient-birthTime",
                        (ca.uhn.fhir.model.primitive.DateTimeDt) param
                              .get(index));
         }
      }
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public List<HumanNameDt> getName()
   {
      return adaptedClass.getName();
   }

   public void setName(List<HumanNameDt> param)
   {
      adaptedClass.setName(param);
   }

   public void addName(HumanNameDt param)
   {
      adaptedClass.getName().add(param);
   }

   public List<ContactPointDt> getTelecom()
   {
      return adaptedClass.getTelecom();
   }

   public void setTelecom(List<ContactPointDt> param)
   {
      adaptedClass.setTelecom(param);
   }

   public void addTelecom(ContactPointDt param)
   {
      adaptedClass.getTelecom().add(param);
   }

   public String getGender()
   {
      return adaptedClass.getGender();
   }

   public void setGender(String param)
   {
      adaptedClass.setGender(AdministrativeGenderEnum.valueOf(param));
   }

   public BoundCodeDt<AdministrativeGenderEnum> getGenderElement()
   {
      return adaptedClass.getGenderElement();
   }

   public void setGender(BoundCodeDt<AdministrativeGenderEnum> param)
   {
      adaptedClass.setGender(param);
   }

   public DateDt getBirthDateElement()
   {
      return adaptedClass.getBirthDateElement();
   }

   public Date getBirthDate()
   {
      return adaptedClass.getBirthDate();
   }

   public void setBirthDate(Date param)
   {
      adaptedClass
            .setBirthDate(new ca.uhn.fhir.model.primitive.DateDt(param));
   }

   public void setBirthDate(DateDt param)
   {
      adaptedClass.setBirthDate(param);
   }

   public BooleanDt getDeceasedBooleanElement()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getDeceased();
      }
      else
      {
         return null;
      }
   }

   public Boolean getDeceasedBoolean()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getDeceased()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setDeceasedBoolean(BooleanDt param)
   {
      adaptedClass.setDeceased(param);
   }

   public void setDeceasedBoolean(Boolean param)
   {
      adaptedClass.setDeceased(new ca.uhn.fhir.model.primitive.BooleanDt(
            param));
   }

   public DateTimeDt getDeceasedDateTimeElement()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getDeceased();
      }
      else
      {
         return null;
      }
   }

   public Date getDeceasedDateTime()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getDeceased()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setDeceasedDateTime(DateTimeDt param)
   {
      adaptedClass.setDeceased(param);
   }

   public void setDeceasedDateTime(Date param)
   {
      adaptedClass.setDeceased(new ca.uhn.fhir.model.primitive.DateTimeDt(
            param));
   }

   public List<AddressDt> getAddress()
   {
      return adaptedClass.getAddress();
   }

   public void setAddress(List<AddressDt> param)
   {
      adaptedClass.setAddress(param);
   }

   public void addAddress(AddressDt param)
   {
      adaptedClass.getAddress().add(param);
   }

   public BoundCodeableConceptDt<MaritalStatusCodesEnum> getMaritalStatus()
   {
      return adaptedClass.getMaritalStatus();
   }

   public void setMaritalStatus(
         BoundCodeableConceptDt<MaritalStatusCodesEnum> param)
   {
      adaptedClass.setMaritalStatus(param);
   }

   public BooleanDt getMultipleBirthBooleanElement()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getMultipleBirth();
      }
      else
      {
         return null;
      }
   }

   public Boolean getMultipleBirthBoolean()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getMultipleBirth()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setMultipleBirthBoolean(BooleanDt param)
   {
      adaptedClass.setMultipleBirth(param);
   }

   public void setMultipleBirthBoolean(Boolean param)
   {
      adaptedClass
            .setMultipleBirth(new ca.uhn.fhir.model.primitive.BooleanDt(
                  param));
   }

   public IntegerDt getMultipleBirthIntegerElement()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.IntegerDt)
      {
         return (ca.uhn.fhir.model.primitive.IntegerDt) adaptedClass
               .getMultipleBirth();
      }
      else
      {
         return null;
      }
   }

   public Integer getMultipleBirthInteger()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.IntegerDt)
      {
         return ((ca.uhn.fhir.model.primitive.IntegerDt) adaptedClass
               .getMultipleBirth()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setMultipleBirthInteger(IntegerDt param)
   {
      adaptedClass.setMultipleBirth(param);
   }

   public void setMultipleBirthInteger(Integer param)
   {
      adaptedClass
            .setMultipleBirth(new ca.uhn.fhir.model.primitive.IntegerDt(
                  param));
   }

   public List<AttachmentDt> getPhoto()
   {
      return adaptedClass.getPhoto();
   }

   public void setPhoto(List<AttachmentDt> param)
   {
      adaptedClass.setPhoto(param);
   }

   public void addPhoto(AttachmentDt param)
   {
      adaptedClass.getPhoto().add(param);
   }

   public BooleanDt getActiveElement()
   {
      return adaptedClass.getActiveElement();
   }

   public Boolean getActive()
   {
      return adaptedClass.getActive();
   }

   public void setActive(Boolean param)
   {
      adaptedClass
            .setActive(new ca.uhn.fhir.model.primitive.BooleanDt(param));
   }

   public void setActive(BooleanDt param)
   {
      adaptedClass.setActive(param);
   }

   public Patient getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(Patient param)
   {
      this.adaptedClass = param;
   }
}