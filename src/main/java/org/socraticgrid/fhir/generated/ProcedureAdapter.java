package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.IProcedure;
import ca.uhn.fhir.model.dstu2.resource.Procedure;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Location;
import ca.uhn.fhir.model.primitive.StringDt;

public class ProcedureAdapter implements IProcedure
{

   private Procedure adaptedClass = new Procedure();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public Patient getPatientResource()
   {
      if (adaptedClass.getPatient().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getPatient().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPatientResource(Patient param)
   {
      adaptedClass.getPatient().setResource(param);
   }

   public BoundCodeDt<ProcedureStatusEnum> getStatusElement()
   {
      return adaptedClass.getStatusElement();
   }

   public String getStatus()
   {
      return adaptedClass.getStatus();
   }

   public void setStatus(String param)
   {
      adaptedClass
            .setStatus(ca.uhn.fhir.model.dstu2.valueset.ProcedureStatusEnum
                  .valueOf(param));
   }

   public void setStatus(BoundCodeDt<ProcedureStatusEnum> param)
   {
      adaptedClass.setStatus(param);
   }

   public CodeableConceptDt getCategory()
   {
      return adaptedClass.getCategory();
   }

   public void setCategory(CodeableConceptDt param)
   {
      adaptedClass.setCategory(param);
   }

   public List<String> getCategoryAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getCategory().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public CodeableConceptDt getType()
   {
      return adaptedClass.getType();
   }

   public void setType(CodeableConceptDt param)
   {
      adaptedClass.setType(param);
   }

   public List<String> getTypeAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getType().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public List<Procedure.BodySite> getBodySite()
   {
      return adaptedClass.getBodySite();
   }

   public void setBodySite(List<Procedure.BodySite> param)
   {
      adaptedClass.setBodySite(param);
   }

   public void addBodySite(Procedure.BodySite param)
   {
      adaptedClass.getBodySite().add(param);
   }

   public List<CodeableConceptDt> getIndication()
   {
      return adaptedClass.getIndication();
   }

   public void setIndication(List<CodeableConceptDt> param)
   {
      adaptedClass.setIndication(param);
   }

   public void addIndication(CodeableConceptDt param)
   {
      adaptedClass.getIndication().add(param);
   }

   public List<Procedure.Performer> getPerformer()
   {
      return adaptedClass.getPerformer();
   }

   public void setPerformer(List<Procedure.Performer> param)
   {
      adaptedClass.setPerformer(param);
   }

   public void addPerformer(Procedure.Performer param)
   {
      adaptedClass.getPerformer().add(param);
   }

   public DateTimeDt getPerformedDateTimeElement()
   {
      if (adaptedClass.getPerformed() != null
            && adaptedClass.getPerformed() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getPerformed();
      }
      else
      {
         return null;
      }
   }

   public Date getPerformedDateTime()
   {
      if (adaptedClass.getPerformed() != null
            && adaptedClass.getPerformed() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getPerformed()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setPerformedDateTime(DateTimeDt param)
   {
      adaptedClass.setPerformed(param);
   }

   public void setPerformedDateTime(Date param)
   {
      adaptedClass.setPerformed(new ca.uhn.fhir.model.primitive.DateTimeDt(
            param));
   }

   public TimingDt getPerformedPeriod()
   {
      if (adaptedClass.getPerformed() != null
            && adaptedClass.getPerformed() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getPerformed();
      }
      else
      {
         return null;
      }
   }

   public void setPerformedPeriod(TimingDt param)
   {
      adaptedClass.setPerformed(param);
   }

   public Encounter getEncounterResource()
   {
      if (adaptedClass.getEncounter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Encounter)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Encounter) adaptedClass
               .getEncounter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setEncounterResource(Encounter param)
   {
      adaptedClass.getEncounter().setResource(param);
   }

   public Location getLocationResource()
   {
      if (adaptedClass.getLocation().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Location)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Location) adaptedClass
               .getLocation().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setLocationResource(Location param)
   {
      adaptedClass.getLocation().setResource(param);
   }

   public CodeableConceptDt getOutcome()
   {
      return adaptedClass.getOutcome();
   }

   public void setOutcome(CodeableConceptDt param)
   {
      adaptedClass.setOutcome(param);
   }

   public List<String> getOutcomeAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getOutcome().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public List<CodeableConceptDt> getComplication()
   {
      return adaptedClass.getComplication();
   }

   public void setComplication(List<CodeableConceptDt> param)
   {
      adaptedClass.setComplication(param);
   }

   public void addComplication(CodeableConceptDt param)
   {
      adaptedClass.getComplication().add(param);
   }

   public List<CodeableConceptDt> getFollowUp()
   {
      return adaptedClass.getFollowUp();
   }

   public void setFollowUp(List<CodeableConceptDt> param)
   {
      adaptedClass.setFollowUp(param);
   }

   public void addFollowUp(CodeableConceptDt param)
   {
      adaptedClass.getFollowUp().add(param);
   }

   public List<Procedure.RelatedItem> getRelatedItem()
   {
      return adaptedClass.getRelatedItem();
   }

   public void setRelatedItem(List<Procedure.RelatedItem> param)
   {
      adaptedClass.setRelatedItem(param);
   }

   public void addRelatedItem(Procedure.RelatedItem param)
   {
      adaptedClass.getRelatedItem().add(param);
   }

   public StringDt getNotesElement()
   {
      return adaptedClass.getNotesElement();
   }

   public String getNotes()
   {
      return adaptedClass.getNotes();
   }

   public void setNotes(String param)
   {
      adaptedClass.setNotes(new ca.uhn.fhir.model.primitive.StringDt(param));
   }

   public void setNotes(StringDt param)
   {
      adaptedClass.setNotes(param);
   }

   public List<Procedure.Device> getDevice()
   {
      return adaptedClass.getDevice();
   }

   public void setDevice(List<Procedure.Device> param)
   {
      adaptedClass.setDevice(param);
   }

   public void addDevice(Procedure.Device param)
   {
      adaptedClass.getDevice().add(param);
   }

   public Procedure getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(Procedure param)
   {
      this.adaptedClass = param;
   }
}