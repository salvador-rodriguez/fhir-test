package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.AddressDt;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.ContactPointDt;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DateDt;
import java.util.Date;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.dstu2.valueset.MaritalStatusCodesEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.primitive.IntegerDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;

public interface IQICorePatient
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public List<CodeableConceptDt> getRace();

   public void setRace(List<CodeableConceptDt> param);

   public CodeableConceptDt getEthnicity();

   public void setEthnicity(CodeableConceptDt param);

   public CodeableConceptDt getReligion();

   public void setReligion(CodeableConceptDt param);

   public AddressDt getBirthPlace();

   public void setBirthPlace(AddressDt param);

   public List<CodeableConceptDt> getDisability();

   public void setDisability(List<CodeableConceptDt> param);

   public CodeableConceptDt getImportance();

   public void setImportance(CodeableConceptDt param);

   public List<Nationality> getNationality();

   public void setNationality(List<Nationality> param);

   public List<Citizenship> getCitizenship();

   public void setCitizenship(List<Citizenship> param);

   public BooleanDt getCadavericDonor();

   public void setCadavericDonor(BooleanDt param);

   public CodeableConceptDt getVeteranMilitaryStatus();

   public void setVeteranMilitaryStatus(CodeableConceptDt param);

   public CodeableConceptDt getAdoptionInfo();

   public void setAdoptionInfo(CodeableConceptDt param);

   public CodeableConceptDt getCauseOfDeath();

   public void setCauseOfDeath(CodeableConceptDt param);

   public List<ClinicalTrial> getClinicalTrial();

   public void setClinicalTrial(List<ClinicalTrial> param);

   public List<DateTimeDt> getBirthTime();

   public void setBirthTime(List<DateTimeDt> param);

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public List<HumanNameDt> getName();

   public void setName(List<HumanNameDt> param);

   public void addName(HumanNameDt param);

   public List<ContactPointDt> getTelecom();

   public void setTelecom(List<ContactPointDt> param);

   public void addTelecom(ContactPointDt param);

   public String getGender();

   public void setGender(String param);

   public BoundCodeDt<AdministrativeGenderEnum> getGenderElement();

   public void setGender(BoundCodeDt<AdministrativeGenderEnum> param);

   public DateDt getBirthDateElement();

   public Date getBirthDate();

   public void setBirthDate(Date param);

   public void setBirthDate(DateDt param);

   public BooleanDt getDeceasedBooleanElement();

   public Boolean getDeceasedBoolean();

   public void setDeceasedBoolean(BooleanDt param);

   public void setDeceasedBoolean(Boolean param);

   public DateTimeDt getDeceasedDateTimeElement();

   public Date getDeceasedDateTime();

   public void setDeceasedDateTime(DateTimeDt param);

   public void setDeceasedDateTime(Date param);

   public List<AddressDt> getAddress();

   public void setAddress(List<AddressDt> param);

   public void addAddress(AddressDt param);

   public BoundCodeableConceptDt<MaritalStatusCodesEnum> getMaritalStatus();

   public void setMaritalStatus(
         BoundCodeableConceptDt<MaritalStatusCodesEnum> param);

   public BooleanDt getMultipleBirthBooleanElement();

   public Boolean getMultipleBirthBoolean();

   public void setMultipleBirthBoolean(BooleanDt param);

   public void setMultipleBirthBoolean(Boolean param);

   public IntegerDt getMultipleBirthIntegerElement();

   public Integer getMultipleBirthInteger();

   public void setMultipleBirthInteger(IntegerDt param);

   public void setMultipleBirthInteger(Integer param);

   public List<AttachmentDt> getPhoto();

   public void setPhoto(List<AttachmentDt> param);

   public void addPhoto(AttachmentDt param);

   public BooleanDt getActiveElement();

   public Boolean getActive();

   public void setActive(Boolean param);

   public void setActive(BooleanDt param);

   public Patient getAdaptee();

   public void setAdaptee(Patient param);
}