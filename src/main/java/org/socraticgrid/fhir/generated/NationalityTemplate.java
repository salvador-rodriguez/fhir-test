package org.socraticgrid.fhir.generated;

import java.util.List;

import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.BaseResource;

public class NationalityTemplate {

	public static final String uri = "http://hl7.org/fhir/StructureDefinition/patient-nationality";
	public static final String codeUri = "http://hl7.org/fhir/StructureDefinition/patient-nationality-code";
	public static final String periodUri = "http://hl7.org/fhir/StructureDefinition/patient-nationality-period";
	private ExtensionDt rootObjectExtension;

	public CodeableConceptDt getCode() {
		CodeableConceptDt returnValue;
		List<ExtensionDt> extensions = rootObjectExtension
				.getUndeclaredExtensionsByUrl(codeUri);
		if (extensions.size() == 1) {
			returnValue = (CodeableConceptDt) extensions.get(0).getValue();
		} else if (extensions.size() == 0) {
			returnValue = null;
		} else {
			throw new IllegalStateException(
					"More than one code specified for this object.");
		}
		return returnValue;
	}

	public void setCode(CodeableConceptDt param) {
		List<ExtensionDt> extensions = rootObjectExtension
				.getUndeclaredExtensionsByUrl(codeUri);
		if (extensions.size() == 1) {
			extensions.get(0).setValue(param);
		} else if (extensions.size() == 0) {
			ExtensionDt newExtension = new ExtensionDt(false, codeUri, param);
			rootObjectExtension.addUndeclaredExtension(newExtension);
		} else {
			throw new IllegalStateException(
					"More than one code specified for this object.");
		}
	}

	public TimingDt getPeriod() {
		TimingDt returnValue;
		List<ExtensionDt> extensions = rootObjectExtension
				.getUndeclaredExtensionsByUrl(periodUri);
		if (extensions.size() == 1) {
			returnValue = (TimingDt) extensions.get(0).getValue();
		} else if (extensions.size() == 0) {
			returnValue = null;
		} else {
			throw new IllegalStateException(
					"More than one code specified for this object.");
		}
		return returnValue;
	}

	public void setPeriod(TimingDt param) {
		List<ExtensionDt> extensions = rootObjectExtension
				.getUndeclaredExtensionsByUrl(periodUri);
		if (extensions.size() == 1) {
			extensions.get(0).setValue(param);
		} else if (extensions.size() == 0) {
			ExtensionDt newExtension = new ExtensionDt(false, periodUri, param);
			rootObjectExtension.addUndeclaredExtension(newExtension);
		} else {
			throw new IllegalStateException(
					"More than one code specified for this object.");
		}
	}

	public ExtensionDt getRootObjectExtension() {
		return rootObjectExtension;
	}

	public void setRootObjectExtension(ExtensionDt parentExtension) {
		this.rootObjectExtension = parentExtension;
	}
	
	public ExtensionDt bindExtensionToParent(BaseResource containingResource) {
		rootObjectExtension = new ExtensionDt(false, uri);
		containingResource.addUndeclaredExtension(rootObjectExtension);
		return rootObjectExtension;
	}

}