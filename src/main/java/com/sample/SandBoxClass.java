package com.sample;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.socraticgrid.fhir.generated.Citizenship;
import org.socraticgrid.fhir.generated.QICorePatientAdapter;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.parser.IParser;

public class SandBoxClass {

	void test() {

		QICorePatientAdapter patient = new QICorePatientAdapter();

		CodeableConceptDt religion = new CodeableConceptDt();

		CodingDt religionCode = new CodingDt("http://thedevine", "Spiritual");

		religion.getCoding().add(religionCode);

		patient.setReligion(religion);

		patient.setActive(true);

		patient.setBirthDate(new Date());

		List<Citizenship> citizenships = new ArrayList<Citizenship>();

		Citizenship citizenship = new Citizenship();

		CodeableConceptDt codeableConcept = new CodeableConceptDt();

		CodingDt coding = new CodingDt("http://snomedct", "234");

		coding.setDisplay("My first code");

		codeableConcept.getCoding().add(coding);

		citizenship.setCode(codeableConcept);

		TimingDt timing = new TimingDt();

		timing.addEvent(new Date());

		citizenship.setPeriod(timing);

		citizenships.add(citizenship);

		patient.setCitizenship(citizenships);

		FhirContext context = FhirContext.forDstu2();

		IParser parser = context.newXmlParser();

		String patientXml = parser.encodeResourceToString(patient.getAdaptee());

		System.out.println(patientXml);
		
		QICorePatientAdapter qiCorePatient = new QICorePatientAdapter();
		List<Citizenship> listCitizenship = qiCorePatient.getCitizenship();
		Citizenship citizen = listCitizenship.get(0);
		
	}

}
